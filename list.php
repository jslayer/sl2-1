<?php
require "vendor/autoload.php";

define('DEVELOPER_KEY', 'AIzaSyAhl611TB_itoXxMro7Ph3exvjzpPuV-K0');
define('YOUTUBE_SEARCH_TYPE_VIDEO', 'video');

$log = new Monolog\Logger('name');
$log->pushHandler(new Monolog\Handler\FirePHPHandler());

$search = array(
    'string' => 'php composer',
    'limit'  => 10
);

$searchString = 'php composer';

$client = new Google_Client();
$client->setDeveloperKey(DEVELOPER_KEY);

$youtube = new Google_YouTubeService($client);

//$apiConfig['use_objects'] = TRUE;

$searchResponse = array(
    'pageInfo' => array(
        'totalResults' => 0
    ),
    'items' => array()
);

try {
    $searchResponse = $youtube->search->listSearch(
        'id,snippet',
        array(
             'q'          => $search['string'],
             'maxResults' => $search['limit'],
             'type'       => YOUTUBE_SEARCH_TYPE_VIDEO
        )
    );
} catch (Google_ServiceException $e) {
    $log->error($e->getMessage());
} catch (Google_Exception $e) {
    $log->error($e->getMessage());
}

$log->addInfo('Total results: ' . $searchResponse['pageInfo']['totalResults']);

?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="vendor/twitter/bootstrap/docs/assets/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/twitter/bootstrap/docs/assets/css/bootstrap-responsive.css" />
    <style type="text/css">
        .video-result {}
        .video-result .thumbnail {
            position : relative;
            display : block;
            padding : 0;
        }
        .video-result img {
            opacity: 0.6;
            border : none;
        }
        .video-result .title {
            position : absolute;
            left : 0;
            bottom : 0;
            padding : 0.5em 2%;
            background : rgba(0, 0, 0, 0.7);
            font-size : 14px;
            font-weight : bold;
            color : #fff;
            width : 96%;
            display : block;
        }
        .video-result:hover img {
            opacity: 1;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="hero-unit">
        <h1>Symphony 2; Lesson 1</h1>
    </div>

    <?php if (empty($searchResponse['items'])) { ?>
      <p>Result set is empty</p>
    <?php } else { ?>
        <ul class="thumbnails">
            <?php foreach($searchResponse['items'] as $item) { ?>
                <?php
                $snippet = $item['snippet'];
                ?>
                <li class="span4 video-result">
                    <a href="http://youtu.be/<?php print $item['id']['videoId'] ?>" target="_blank" class="thumbnail">
                        <img src="<?php print $snippet['thumbnails']['high']['url']; ?>" alt="<?php print $snippet['title']; ?>" />
                        <span class="title"><?php print $snippet['title']; ?></span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>


</div>
</body>
</html>
